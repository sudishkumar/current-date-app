package com.example.dateapp

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.time.days

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val calender = Calendar.getInstance()
        val year = calender.get(Calendar.YEAR)
        val month = calender.get(Calendar.MONTH) + 1
        val dayOfMonth = calender.get(Calendar.DAY_OF_MONTH)
        val day = calender.get(Calendar.DAY_OF_MONTH)
        val date = "$year-0$month-$day"
        val allDaysInMonth = calender.getActualMaximum(Calendar.DATE)

        Toast.makeText(this, "The current date or today  is $date", Toast.LENGTH_SHORT).show()

        // Adding and subtracting five days  in today date
        val addFiveDaysExtra = day + 20
        // one thing should be remember  we will check is date is greater than month day of month or  or not it or days may be
        if (addFiveDaysExtra > allDaysInMonth ){
            val addOneInMonth = month + 1
            val subDate = addFiveDaysExtra - allDaysInMonth
            val filterDate = "$year-$addOneInMonth-$subDate"
            Toast.makeText(this, "Five days extra in the current date or today  is $filterDate", Toast.LENGTH_SHORT).show()
        }else{
            val fiveDaysExtraDate = "$year-$month-$addFiveDaysExtra"
            // one thing should be remember  we will check is date is greater than month day of month or  or not
            Toast.makeText(this, "Five days extra in the current date or today  is $fiveDaysExtraDate", Toast.LENGTH_SHORT).show()
        }
        val fiveDaysLess = day - 5
        val fiveDaysLessDate = "$year-$month-$fiveDaysLess"
//        Toast.makeText(this, "Five days less in the current date or today  is $fiveDaysLessDate", Toast.LENGTH_SHORT).show()
    }
}